
<?php 
get_header();
/* Template Name: Blog */


?>
<div id="content" class="site-content">
			<div class="page-header" style="background: url(<?php bloginfo('template_directory');?>/images/header-bg.jpg) no-repeat;">
				<div class="cm-wrapper">
					<h1 class="page-title">Content Marketing Articles</h1>
					<a href="#primary" class="scroll-down"></a>
				</div>
			</div>
			<?php 
			query_posts( ' ' );
			if(have_posts() ):?>
			<div class="cm-wrapper">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						
						<div class="article-group grid-layout">

							<?php
							 while(have_posts() ): the_post();?>
								<article itemscope itemtype="http://schema.org/Article">
									<header class="entry-header">
										<h2 class="entry-title" itemprop="headline"><a href="#"><?php the_title(); ?></a></h2>
										<div class="entry-meta">
											<span class="posted-on" itemprop="datePublished dateModified">
												<span>Last Updated:</span> <a href="#">
													<?php content_marketing_posted_on();?>
												</a>
											</span>
											<span class="byline" itemprop="author">
												 <span class="author vcard">
													<a href="#" class="url" itemprop="name"><?php content_marketing_posted_by(); ?></a>
												</span>
											</span>
											<span class="comment-box">
												<span class="comment-count">5</span> Comments
											</span>
										</div>
									</header>
									<div class="entry-content">
										<a href="#" class="entry-image" itemprop="thumbnailUrl"><?php the_post_thumbnail(); ?></a>
										<p><?php the_excerpt(); ?></p>
										<a href="#" class="readmore" itemprop="MainEntityOfPage">Continue Reading <img src="<?php bloginfo('template_directory');?>/images/readmore-arrow.png" alt="readmore arrow"></a>
									</div>
								</article>
							<?php endwhile; ?>
							
						</div>
					
<?php endif; ?>

					</main>
				</div>
			</div>
		</div>


