<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package content-marketing
 */

get_header(); 
$theme_layout = get_theme_mod('contenet_marketing_layout_set', 'default-layout');
	?>
	<div id="content" class="site-content">
		<?php content_marketing_single_page_banner(); ?>
			<div class="cm-wrapper">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<div class="article-group <?php echo $theme_layout; ?>">

							<?php
							if ( have_posts() ) : 
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();

								/*
								 * Include the Post-Type-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content', get_post_type() );

							endwhile;

							the_posts_navigation();

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>

					</div>

					
				</main>
				
			</div>
			
			<?php if($theme_layout == 'default-layout' ):
				get_sidebar();
			endif; ?>
		</div>

	</div>

<?php

get_footer();
