<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package content-marketing
 */
		$content_marketing_copyright = get_theme_mod('set_copyright', __('© Copyright 2017, Content Marketing. All Rights Reserved. Theme by Rara Theme. Powered by WordPress.', 'content-marketing'));
		?>

	<footer class="site-footer" itemscope itemtype="http://schema.org/WPFooter">
				<div class="top-footer">
					<div class="cm-wrapper">
						<section class="widget widget_text">
							<div class="textwidget">
								<p><img src="<?php bloginfo('template_directory');?>/images/theme-logo-sm.png" alt="theme logo"></p>
								<p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human DNA — without turning up a single bone. Their new technique, described in a study published on Thursday in the journal</p>
							</div>
						</section>
						<section class="widget widget_nav_menu">
							<h3 class="widget-title">Quick Links</h3>
							<ul class="menu">
								<li><a href="#">Home</a></li>
								<li><a href="#">About us</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Services</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
						</section>
						<section class="widget widget_recent_entries">
							<h3 class="widget-title">Recent Posts</h3>
							<ul>
								<li>
									<a href="#">Content Marketing Trends to Watch for 2018</a>
									<span class="post-date">12 Oct 2017</span>
								</li>
								<li>
									<a href="#">Content Marketing Trends to Watch for 2018</a>
									<span class="post-date">12 Oct 2017</span>
								</li>
							</ul>
						</section>
						<section class="widget widget_social_icons">
							<h3 class="widget-title">Follow Us</h3>
							<ul>
								<li>
									<a href="#"><i class="fa fa-facebook"></i> <span>Facebook</span></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i> <span>Twitter</span></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-google-plus"></i> <span>Google+</span></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-linkedin"></i> <span>LinkedIn</span></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagram"></i> <span>Instagram</span></a>
								</li>
							</ul>
						</section>
					</div>
				</div>
				<div class="bottom-footer">
					<div class="cm-wrapper">
						<div class="copyright">
							<?php echo $content_marketing_copyright; ?> 
						</div>
						<div class="scroll-to-top">
							<span>back to top <img src="<?php bloginfo('template_directory');?>/images/scroll-top-arrow.png" alt="scroll to top"></span>
						</div>
					</div>
				</div>
	</footer><!-- .site-footer -->
</div>

	<!-- JS FILES -->
	<?php wp_footer(); ?>
</body>
</html>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
