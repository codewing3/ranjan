<?php
/**
 * content-marketing functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package content-marketing
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'content_marketing_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function content_marketing_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on content-marketing, use a find and replace
		 * to change 'content-marketing' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'content-marketing', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary-menu' => __('Primary', 'content-marketing'),
				'footer-menu' => __('Footer', 'content-marketing'),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_image_size( 'abcde', 389, 412, true );


		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'content_marketing_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'content_marketing_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function content_marketing_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'content_marketing_content_width', 640 );
}
add_action( 'after_setup_theme', 'content_marketing_content_width', 0 );

/**
 * Widgets codes.
 */
require get_template_directory() . '/inc/widgets.php';


/**
 * Enqueue scripts and styles.
 */


function content_marketing_scripts() {
	wp_enqueue_style( 'content-marketing-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'content-marketing-style', 'rtl', 'replace' );
	wp_enqueue_style('content-marketing-font-awesome', get_template_directory_uri(). '/css/font-awesome.min.css', array(), '4.7.0');
	// wp_enqueue_style('content-marketing-font-awesome', get_template_directory_uri(). '/style.css', array());
	wp_enqueue_style('content-marketing-google-font',  'https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700|Source+Sans+Pro:400,400i', false);


	wp_enqueue_script('content-marketing-custom-js', get_template_directory_uri(). '/js/custom.js', array('jquery'), _S_VERSION, true);
	wp_enqueue_script( 'content-marketing-jquery112', get_template_directory_uri() . '/js/jquery-1.12.0.js', array(), _S_VERSION, true );

	wp_enqueue_script( 'content-marketing-counterup', get_template_directory_uri() . '/js/jquery.counterup.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'content-marketing-waypoint', get_template_directory_uri() . '/js/waypoints-2.0.3.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'content_marketing_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';
require get_template_directory() . '/inc/customizer/blog-customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

