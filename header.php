<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package content-marketing
 */

?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>.::GROW AND CONVERT | HOMEPAGE::.</title>
	<?php wp_head(); ?>
</head>
<body itemscope itemtype="http://schema.org/webPage" <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php 

	$main_banner = get_theme_mod('content_marketing_index_banner_set', (get_stylesheet_directory_uri() . '/images/banner-img1.jpg'));
	$banner_wrapper = get_theme_mod('content_marketing_banner_description_set', (get_template_directory_uri() . '/images/ban-caption.png'));
	$button_name = get_theme_mod('content_marketing_top_header_button_set', __('REGISTER TODAY', 'content-marketing'));

	$top_header_des = get_theme_mod('content_marketing_top_header_desc_set', __('The ultimate e-mail marketing course is out now! ', 'content-marketing'));
	?>
	<div id="page" class="site">
		<?php if( is_home() ): ?>
		<header class="site-header header-1" itemscope itemtype="http://schema.org/WPHeader">
		<?php else: ?>
			<header class="site-header" itemscope itemtype="http://schema.org/WPHeader">
		<?php endif; ?>
			<div class="header-ticker">
				<span class="ticker-close"><i class="fa fa-close"></i></span>
				<div class="ticker-title-wrap">
					<div class="cm-wrapper">
						<p class="ticker-title">
							<?php echo $top_header_des; ?>
							<a href="#"><?php echo $button_name; ?></a>
						</p>
					</div>
				</div>
			</div><!-- .header-ticker -->
			<div class="main-header">
				<div class="cm-wrapper">
					<div class="site-branding">
						<div class="site-logo">							
							<?php the_custom_logo(); ?>							
						</div>
					</div>
					<div class="nav-wrap">
						<nav class="main-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<button class="toggle-button" type="button">
								<span class="toggle-bar"></span>
								<span class="toggle-bar"></span>
								<span class="toggle-bar"></span>
							</button>
							<?php wp_nav_menu(
								array(
									'theme-location' => 'primary',
									'menu-id' => 'header-1'
								)
							); ?>
						</nav>
					</div>
				</div>
			</div><!-- .main-header -->
		</header><!-- .site-header -->
		<?php if( is_front_page() ): ?>
			<div class="banner-section">
				<div class="banner-img">
					<div class="ban-img-holder" style="background: url(<?php echo $main_banner; ?>) no-repeat;"></div>
					<div class="cm-wrapper">
						<?php if($banner_wrapper): ?>
								<img src="<?php echo $banner_wrapper; ?>" alt="banner caption">
						<?php endif ?>
					</div>
					<a href="#clients" class="scroll-down"></a>
				</div>
			</div><!-- .banner-section -->
		<?php endif;?>