<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package content-marketing
 */
$theme_layout = get_theme_mod('contenet_marketing_layout_set', 'default-layout');

?>


<?php 
if($theme_layout =='grid-layout'):
	content_marketing_grid_layout(); 
else:
	content_marketing_default_layout(); 


endif; ?>							