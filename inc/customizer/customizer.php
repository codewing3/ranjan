<?php
/**
 * content-marketing Theme Customizer
 *
 * @package content-marketing
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
       
//file input sanitization function
 
function content_marketing_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'content_marketing_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'content_marketing_customize_partial_blogdescription',
			)
		);
	}
}
add_action( 'customize_register', 'content_marketing_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function content_marketing_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function content_marketing_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function content_marketing_customize_preview_js() {
	wp_enqueue_script( 'content-marketing-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'content_marketing_customize_preview_js' );

function theme_slug_customizer( $wp_customize ) {           
  
    //your section
    $wp_customize->add_section( 
        'content_marketing_index_page_section', 
        array(
            'title' => __( 'Index Page Banner', 'content-marketing' ),
            'priority' => 150
        )
    );      
          
     
    //add select setting to your section
    $wp_customize->add_setting( 
        'content_marketing_index_banner_set', 
        array(
            'default'       => get_stylesheet_directory_uri() . '/images/banner-img1.jpg',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
      
      
    $wp_customize->add_control( 
        new WP_Customize_Upload_Control( 
            $wp_customize, 
            'content_marketing_index_banner_set', 
            array(
                'label'      => __( 'Index Page Main Banner', 'content-marketing' ),
                'section'    => 'content_marketing_index_page_section'                   
            )
        ) 
    );  

    //add select setting to your section
    $wp_customize->add_setting( 
        'content_marketing_banner_description_set', 
        array(
            'default'       => get_stylesheet_directory_uri() . '/images/ban-caption.png',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
      
      
    $wp_customize->add_control( 
        new WP_Customize_Upload_Control( 
            $wp_customize, 
            'content_marketing_banner_description_set', 
            array(
                'label'      => __( 'Index Page Banner Small Image', 'content-marketing' ),
                'section'    => 'content_marketing_index_page_section'                   
            )
        ) 
    );  

    $wp_customize -> add_setting(
    'content_marketing_top_header_desc_set',array(        
        'default' => __('The ultimate e-mail marketing course is out now!', 'content-marketing'),
        'sanitize_callback' => 'sanitize_text_field',
       // 'transport' => 'postMessage'
        )
    );
    $wp_customize -> add_control(
    'content_marketing_top_header_desc_set',array(
        'label' => __('Top Header Short Description About Register', 'content-marketing'),
        'section' => 'content_marketing_index_page_section',
        'type' => 'text',
        

        )
    ); 

     $wp_customize -> add_setting(
    'content_marketing_top_header_button_set',array(        
        'default' => __('REGISTER TODAY', 'content-marketing'),
        'sanitize_callback' => 'sanitize_text_field',
       
        )
    );
    $wp_customize -> add_control(
    'content_marketing_top_header_button_set',array(
        'label' => __('Button Name', 'content-marketing'),
        'section' => 'content_marketing_index_page_section',
        'type' => 'text',
        

        )
    ); 
     
}
add_action( 'customize_register', 'theme_slug_customizer' );


 //file input sanitization function


function author_callout_section( $wp_customize ) {
		
    // New panel for "Layout".
    $wp_customize->add_section(
        'content_marketing_index_author_sec', array(
        'title' => 'Index Author',
        'priority' => 160,
        //'description' => __('The Author section is only displayed on the Blog page.', 'theminimalist'),
        )
    );

    //title
     $wp_customize -> add_setting(
    'content_marketing_index_author_title_sec',array(
        
        'default' => __('I\'m Kunjan Rajbhandari', 'content-marketing'),
        'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize -> add_control(
    'content_marketing_index_author_title_sec',array(
        'label' => __('Title', 'content-marketing'),
        'section' => 'content_marketing_index_author_sec',
        'type' => 'text',
        )
    ); 


    $wp_customize -> add_setting(
    'content_marketing_index_author_textarea_set',array(        
        'default' => '',
        'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize -> add_control(
    'content_marketing_index_author_textarea_set',array(
        'label' => __('About Author', 'content-marketing'),
        'section' => 'content_marketing_index_author_sec',
        'type' => 'textarea',
        
        )
    ); 
   



      
              
	//for image
    $wp_customize->add_setting( 
        'content_marketing_index_author_img_setting', 
        array(
            'default'       => get_stylesheet_directory_uri() . '/about-img.jpg',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
      
      
    $wp_customize->add_control( 
        new WP_Customize_Upload_Control( 
            $wp_customize, 
            'content_marketing_index_author_img_setting', 
            array(
                'label'      => __( 'Author image', 'content-marketing' ),
                'section'    => 'content_marketing_index_author_sec'                   
            )
        ) 
    );  


         //file input sanitization function
      
    
    
}
add_action( 'customize_register', 'author_callout_section' );


function content_marketing_copyright_footer($wp_customize){
	//footer copyright section
	 $wp_customize -> add_section(
        'sec_copyright',array(
            'title' => __('Copyright Setting',' '),
            'description' => __('Copyright Section','content-marketing')
            )
        );
    $wp_customize -> add_setting(
        'set_copyright',array(
            'default' => __('© Copyright 2017, Content Marketing. All Rights Reserved. Theme by Rara Theme. Powered by WordPress.', 'content-marketing'),
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize -> add_control(
        'set_copyright',array(
            'label' => 'copyright',
            'description' => __('Please, type your copyright info here','content-marketing'),
            'section' => 'sec_copyright',
            'type' => 'text'
            )
        );
}
add_action( 'customize_register', 'content_marketing_copyright_footer' );

//sanitize callback =>
// image -> esc_url_raw
// text -> sanitize_text_field
//textarea -> wp_kses_post