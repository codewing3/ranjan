<?php 
function content_marketing_blog_page_customize($wp_customize){
    //add select setting to your section

        $wp_customize->add_panel( 'content_marketing_blog_panel', array(
          'title' => __( 'Blog Section', 'content-marketing' ),
          'description' => __('Change your banner image, description and blog layout', 'content-marketing'), // Include 
          'priority' => 200, 
        ) );

        $wp_customize->add_section('content_marketing_blog_sec', array(
            'capability'  => 'edit_theme_options',
            'title' => 'Banner Design',
            'priority' => 10,
            'panel'  => 'content_marketing_blog_panel'
        ));
    

        $wp_customize->add_setting( 
            'content_marketing_blog_banner_set', 
            array(
                'default'       => get_stylesheet_directory_uri() . '/images/header-bg.jpg',
                'sanitize_callback' => 'content_marketing_slug_sanitize'
            )
        );
          
          
        $wp_customize->add_control( 
            new WP_Customize_Upload_Control( 
                $wp_customize, 
                'content_marketing_blog_banner_set', 
                array(
                    'label'      => __( 'Blog Page Main Banner', 'content-marketing' ),
                    'section'    => 'content_marketing_blog_sec',
                                  
                )
            ) 
        );  


        $wp_customize -> add_setting(
        'set_blog_banner_description',array(
            'type' => 'theme_mod',
            'default' => __('Content Marketing Articles', 'content-marketing'),
            'sanitize_callback' => 'sanitize_text_field'
        )
        );
        $wp_customize -> add_control(
            'set_blog_banner_description',array(
                'label' => __('Description', 'content-marketing'),
                'description' => __('Please, type your description info here','content-marketing'),
                'section' => 'content_marketing_blog_sec',
                'type' => 'text'
            )
            );

        $wp_customize->add_section('content_marketing_layout_sec', array(
                'capability'  => 'edit_theme_options',
                'title' => 'Layout Design',
                'priority' => 20,
                'panel'  => 'content_marketing_blog_panel'
            ));


         $wp_customize->add_setting(
        'contenet_marketing_layout_set',
        array(
            'default'   => 'default-layout',
            'sanitize_callback' => 'themeslug_sanitize_radio'
        )
    );
     
    $wp_customize->add_control(
        'contenet_marketing_layout_set',
        array(
            'section'  => 'content_marketing_layout_sec',
            'label'    => __('Choose Layout', 'content-marketing'),
            'type'     => 'radio',
            'choices'  => array(
                'default-layout'    => 'Default',
                'grid-layout'   => 'Grid'
            )
        )
    );
         
}add_action( 'customize_register', 'content_marketing_blog_page_customize' );

function themeslug_sanitize_radio( $input, $setting ) {

  // Ensure input is a slug.
  $input = sanitize_key( $input );

  // Get list of choices from the control associated with the setting.
  $choices = $setting->manager->get_control( $setting->id )->choices;

  // If the input is a valid key, return it; otherwise, return the default.
  return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}