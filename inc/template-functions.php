<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package content-marketing
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */


function content_marketing_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	$theme_layout = get_theme_mod('contenet_marketing_layout_set');
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	if (  is_active_sidebar( 'sidebar-1' ) && $theme_layout == 'default-layout' ) {
		$classes[] = 'archive rightsidebar';
	}

	if( $theme_layout == 'grid-layout' ){
		$classes[] = 'no-sidebar';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'content_marketing_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function content_marketing_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'content_marketing_pingback_header' );


function content_marketing_index_page(){
	?>

	<section id="clients" class="client-section">
		<div class="cm-wrapper">
			<section class="widget widget_raratheme_client_logo_widget">            
				<div class="raratheme-client-logo-holder">
					<div class="raratheme-client-logo-inner-holder">
						<div class="image-holder">
							<a href="#" target="_blank">
								<img class="black-white" src="<?php bloginfo('template_directory');?>/images/basekit.png" alt="basekit">
							</a>
						</div>
						<div class="image-holder">
							<a href="#" target="_blank">
								<img class="black-white" src="<?php bloginfo('template_directory');?>/images/hubspot.png" alt="hubspot">
							</a>                                
						</div>
						<div class="image-holder">
							<a href="#" target="_blank">
								<img class="black-white" src="<?php bloginfo('template_directory');?>/images/fomo.png" alt="fomo">
							</a>                                
						</div>
						<div class="image-holder">
							<a href="#" target="_blank">
								<img class="black-white" src="<?php bloginfo('template_directory');?>/images/mention.png" alt="mention">
							</a>                                
						</div>
						<div class="image-holder">
							<a href="#" target="_blank">
								<img class="black-white" src="<?php bloginfo('template_directory');?>/images/protonet.png" alt="protonet">
							</a>                                
						</div>
						<div class="image-holder">
							<a href="#" target="_blank">
								<img class="black-white" src="<?php bloginfo('template_directory');?>/images/looker.png" alt="looker">
							</a>                                
						</div>
					</div>
				</div>
			</section>
		</div>
	</section><!-- .client-section -->

	<?php content_marketing_index_author(); ?>

	<section class="service-section">
		<div class="cm-wrapper">
			<div class="section-widget-wrap">
				<section class="widget widget_text">
					<h2 class="widget-title">Service we provide</h2>
					<div class="textwidget">
						<p>what we do regularly?</p>
					</div>
				</section>
				<section class="widget widget_rrtc_icon_text_widget">        
					<div class="rtc-itw-holder">
						<div class="rtc-itw-inner-holder">
							<div class="text-holder">
								<h2 class="widget-title">Maximize Global Users</h2>
								<div class="content">
									<p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human</p>
								</div>
								<a class="btn-readmore" href="#" target="_blank">Read More</a>                              
							</div>
							<div class="icon-holder">
								<span class="fa fa-bar-chart"></span>
							</div>
						</div>
					</div>
				</section>
				<section class="widget widget_rrtc_icon_text_widget">        
					<div class="rtc-itw-holder">
						<div class="rtc-itw-inner-holder">
							<div class="text-holder">
								<h2 class="widget-title">Target E-business</h2>
								<div class="content">
									<p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human</p>
								</div>
								<a class="btn-readmore" href="#" target="_blank">Read More</a>                              
							</div>
							<div class="icon-holder">
								<span class="fa fa-rocket"></span>
							</div>
						</div>
					</div>
				</section>
				<section class="widget widget_rrtc_icon_text_widget">        
					<div class="rtc-itw-holder">
						<div class="rtc-itw-inner-holder">
							<div class="text-holder">
								<h2 class="widget-title">Maximize Channels</h2>
								<div class="content">
									<p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human</p>
								</div>
								<a class="btn-readmore" href="#" target="_blank">Read More</a>                              
							</div>
							<div class="icon-holder">
								<span class="fa fa-dashboard"></span>
							</div>
						</div>
					</div>
				</section>
				<section class="widget widget_rrtc_icon_text_widget">        
					<div class="rtc-itw-holder">
						<div class="rtc-itw-inner-holder">
							<div class="text-holder">
								<h2 class="widget-title">Exploit Sticky Metrics</h2>
								<div class="content">
									<p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human</p>
								</div>
								<a class="btn-readmore" href="#" target="_blank">Read More</a>                              
							</div>
							<div class="icon-holder">
								<span class="fa fa-trophy"></span>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section><!-- .service-section -->

	<section class="testimonial-section">
		<div class="cm-wrapper">
			<div class="section-widget-wrap">
				<section class="widget widget_text">
					<h2 class="widget-title">Words from our clients</h2>
					<div class="textwidget">
						<p>what our clients speak about us</p>
					</div>
				</section>
				<section class="widget widget_rrtc_testimonial_widget">        
					<div class="rtc-testimonial-holder">
						<div class="rtc-testimonial-inner-holder">
							<div class="img-holder">
								<img src="<?php bloginfo('template_directory');?>/images/audio-author1.jpg" alt="Diana">
							</div>

							<div class="text-holder">
								<div class="testimonial-meta">
									<span class="name">Michael Jordan</span>
									<span class="designation">Chicago Bulls</span>
								</div>                              
								<div class="testimonial-content">
									<p>“I've missed more than 9000 shots in my career. I've lost almost 300 games. 26 times I've been trusted to take the game winning shot and missed. I've failed over and over and over again in my life. And that is why I succeed.”</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="widget widget_rrtc_testimonial_widget">        
					<div class="rtc-testimonial-holder">
						<div class="rtc-testimonial-inner-holder">
							<div class="img-holder">
								<img src="<?php bloginfo('template_directory');?>/images/audio-author4.jpg" alt="Diana">
							</div>

							<div class="text-holder">
								<div class="testimonial-meta">
									<span class="name">Aristotle</span>
									<span class="designation">Microsoft</span>
								</div>                              
								<div class="testimonial-content">
									<p>“First, have a definite, clear practical ideal; a goal, an objective. Second, have the necessary means to achieve your ends; wisdom, money, materials, and methods. Third, adjust all your means to that end.”</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="widget widget_rrtc_testimonial_widget">        
					<div class="rtc-testimonial-holder">
						<div class="rtc-testimonial-inner-holder">
							<div class="img-holder">
								<img src="<?php bloginfo('template_directory');?>/images/audio-author3.jpg" alt="Diana">
							</div>

							<div class="text-holder">
								<div class="testimonial-meta">
									<span class="name">Helen Keller</span>
									<span class="designation">Uber</span>
								</div>                              
								<div class="testimonial-content">
									<p>“When one door of happiness closes, another opens, but often we look so long at the closed door that we do not see the one that has been opened for us.”</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="widget widget_rrtc_testimonial_widget">        
					<div class="rtc-testimonial-holder">
						<div class="rtc-testimonial-inner-holder">
							<div class="img-holder">
								<img src="<?php bloginfo('template_directory');?>/images/audio-author5.jpg" alt="Diana">
							</div>

							<div class="text-holder">
								<div class="testimonial-meta">
									<span class="name">Bob Dylan</span>
									<span class="designation">Apple</span>
								</div>                              
								<div class="testimonial-content">
									<p>“What’s money? A man is a success if he gets up in the morning and goes to bed at night and in between does what he wants to do.” </p>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section><!-- .testimonial-section -->

	<section class="cta-section">
		<div class="cm-wrapper">
			<section class="widget widget_raratheme_companion_cta_widget">        
				<div class="raratheme-cta-container left">
					<h2 class="widget-title">Want to Know More About Us?</h2>
					<div class="text-holder">
						<p>I'd love to share my experience to help you build your business from scratch to reach your goals, which you can't find elsewhere.</p>
						<div class="button-wrap">
							<a href="#" class="btn-cta btn-1">Start Here</a>
						</div>
					</div>
				</div>         
			</section>
		</div>
	</section><!-- .cta-section -->

<?php }

function content_marketing_index_author(){ ?>
	<section class="about-section">
		<div class="cm-wrapper">
			<?php 
				$author_img = get_theme_mod('content_marketing_index_author_img_setting', (get_stylesheet_directory_uri() . '/about-img.jpg'));
					$author_title = get_theme_mod('content_marketing_index_author_title_sec', __('I\'m Sarah Baker', 'content-marketing'));

					$about_author = get_theme_mod('content_marketing_index_author_textarea_set');
					?>
						<section class="widget widget_raratheme_featured_page_widget">                
							<div class="widget-featured-holder left">
								<div class="text-holder">
									<p class="section-subtitle"><?php esc_html_e('ABOUT THE AUTHOR','content-marketing'); ?></p>
									<h2 class="widget-title"><?php echo $author_title; ?></h2>
									<div class="featured_page_content">
										<p><span class="dropcap-letter">Hi!</span> <?php echo wpautop( get_the_author_meta( 'description' ) ); ?></p>
										<a href="#" target="_blank" class="btn-readmore">Read More</a>
									</div>
								</div>
								<div class="img-holder">
									<a target="_blank" href="#">
										<img src=" <?php echo esc_url($author_img); ?>"class="wp-post-image" alt="">
									</a>
								</div>
							</div>        
						</section>
		</div>
	</section><!-- .about-section -->
<?php }

function content_marketing_default_content(){?>
		<div class="cm-wrapper">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<?php if( have_posts() ):?>
						<div class="article-group grid-layout">

							<?php
							 while( have_posts() ): the_post();?>
								<article itemscope itemtype="http://schema.org/Article">
									<header class="entry-header">
										<h2 class="entry-title" itemprop="headline"><a href="#"><?php the_title(); ?></a></h2>
										<div class="entry-meta">
											<span class="posted-on" itemprop="datePublished dateModified">
												<span>Last Updated:</span> <a href="#">
													<?php content_marketing_posted_on();?>
												</a>
											</span>
											<span class="byline" itemprop="author">
												 <span class="author vcard">
													<a href="#" class="url" itemprop="name"><?php content_marketing_posted_by(); ?></a>
												</span>
											</span>
											<span class="comment-box">
												<span class="comment-count">5</span> Comments
											</span>
										</div>
									</header>
									<div class="entry-content">
										<a href="#" class="entry-image" itemprop="thumbnailUrl"><?php the_post_thumbnail(); ?></a>
										<p><?php the_excerpt(); ?></p>
										<a href="#" class="readmore" itemprop="MainEntityOfPage">Continue Reading <img src="<?php bloginfo('template_directory');?>/images/readmore-arrow.png" alt="readmore arrow"></a>
									</div>
								</article>
							<?php endwhile; ?>
							
						</div>
					
							<?php endif; ?>

					</main>
				</div>
			</div>
		</div>

}
					
<?php


}
function content_marketing_single_page_banner(){ 
	$single_banner_image = get_theme_mod('content_marketing_blog_banner_set', esc_url(get_template_directory_uri() . '/images/header-bg.jpg'));
	$single_description = get_theme_mod('set_blog_banner_description',__('Content Marketing Articles', 'content-marketing'));
	?>
	<div class="page-header" style="background: url(<?php echo $single_banner_image; ?>) no-repeat;">
		<div class="cm-wrapper">
			<?php if( is_archive() ):?>
				<h1 class="page-title"><?php  the_category(); ?></h1>
			<?php else:?>
				<h1 class="page-title"><?php echo $single_description; ?></h1>
			<?php endif;?>
			<a href="#primary" class="scroll-down"></a>
		</div>
	</div>
<?php }

function content_marketing_grid_layout(){?>
	<article itemscope itemtype="http://schema.org/Article">
	<a href="#" class="entry-image" itemprop="thumbnailUrl">
		<?php if( has_post_thumbnail() ):
		 	the_post_thumbnail();
		 endif; ?>			
	</a>
	<div class="entry-content">
		<header class="entry-header">
			<h2 class="entry-title" itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="entry-meta">
				<span class="byline" itemprop="author">
					 <span class="author vcard">
						<a href="#" class="url" itemprop="name"><?php content_marketing_posted_by(); ?></a>
					</span>
				</span>
				<span class="comment-box">
					<span class="comment-count">5</span> Comments
				</span>
			</div>
		</header>
		<p><?php the_excerpt(); ?></p>
		<a href="#" class="readmore" itemprop="MainEntityOfPage">Continue Reading</a>
	</div>
</article>

<?php }

function content_marketing_default_layout(){?>
	<article itemscope itemtype="http://schema.org/Article">
		<header class="entry-header">
			<h2 class="entry-title" itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="entry-meta">
				<span class="posted-on" itemprop="datePublished dateModified">
					<span>Last Updated:</span> <a href="#">
						<time class="updated published"><?php content_marketing_posted_on(); ?></time>
					</a>
				</span>
				<span class="byline" itemprop="author">
					<span class="author vcard">
						<a href="#" class="url" itemprop="name"><?php content_marketing_posted_by(); ?></a>
					</span>
				</span>
				<span class="comment-box">
					<span class="comment-count">5</span> Comments
				</span>
			</div>
		</header>
		<div class="entry-content">
			<a href="<?php the_permalink(); ?>" class="entry-image" itemprop="thumbnailUrl">
				<?php if( has_post_thumbnail() ):
				 	the_post_thumbnail(); 
				endif; ?>
					
			</a>
			<p><?php the_excerpt(); ?></p>
			<a href="<?php the_permalink(); ?>" class="readmore" itemprop="MainEntityOfPage">Continue Reading <img src="<?php echo esc_url(get_template_directory_uri() );?>/images/readmore-arrow.png" alt="readmore arrow"></a>
		</div>
	</article>

<?php  }