<?php
	/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function content_marketing_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'content-marketing' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'content-marketing' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar( 
			array(
			'name'          => __( 'Footer Area 1', 'mosaic-internship' ),
			'id'            => 'footer-1',
			'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'mosaic-internship' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( 
			array(
			'name'          => __( 'Footer Area 2', 'mosaic-internship' ),
			'id'            => 'footer-2',
			'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'mosaic-internship' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( 
			array(
			'name'          => __( 'Footer Area 3', 'mosaic-internship' ),
			'id'            => 'footer-3',
			'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'mosaic-internship' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
}
add_action( 'widgets_init', 'content_marketing_widgets_init' );
