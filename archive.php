<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package content-marketing
 */

get_header(); 
$theme_layout = get_theme_mod('contenet_marketing_layout_set', 'default-layout');?>
<div id="content" class="site-content">
	<?php content_marketing_single_page_banner(); ?>
		<div class="cm-wrapper">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<div class="article-group <?php esc_attr_e( $theme_layout ); ?>">
						<?php
						if ( have_posts() ) : 
						/* Start the Loop */
							while ( have_posts() ) :
								the_post();

								/*
								 * Include the Post-Type-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content', get_post_type() );

							endwhile;

						the_posts_navigation();

						else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>

				</div>

				
			</main>
			
		</div>
		
		<?php get_sidebar(); ?>
	</div>

</div>

<?php

get_footer();
